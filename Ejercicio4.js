//pirámide

for (let i = 1; i <= 9; i++) { //bucle de 1 a 9

    var arrayNum = [];          //declarar array cada iteración 1 a 9

    for (let n = 0; n < i; n++) { //bucle crear array con tantos componenetes como el número de iteración
        arrayNum.push(i);
    }
    arrayNumText = arrayNum.join(" "); // al principio lo hice con array pero la salida por consola no quedaba bien; de ahí que use esta instrucción en vez de hacerlo todo con cadena de texto
    console.log(arrayNumText);      //imprimir array  como texto cada iteración 1 a 9
}


